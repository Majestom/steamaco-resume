from django.contrib import admin

from . import models


@admin.register(models.ResumeItem)
class ResumeItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'title', 'company', 'start_date')
    ordering = ('id', 'user', '-start_date')


@admin.register(models.Resume)
class ResumeAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'resume_name')
    ordering = ('user', 'resume_name')
